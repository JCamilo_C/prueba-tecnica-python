import os.path
from app import create_app

application = create_app()
application.debug = application.config['DEBUG']

if __name__ == "__main__":
    application.run(debug=True)
