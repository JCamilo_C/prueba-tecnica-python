from flask import Blueprint

from . import facturas_service

factura = Blueprint(
    "factura",
    __name__
)

from . import facturas_routes