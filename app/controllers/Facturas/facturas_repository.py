from app.models.models import Cliente, Factura, Producto, Factura_Productos

from sqlalchemy import or_
from app import db
from datetime import datetime

def get_all():
    rresponse = [r.serialize for r in Factura.query.all()]
    return rresponse


def get_by_search(search_value):
    rresponse = Factura.query.join(Factura.productos).join(Factura.cliente).filter(
        or_(
            Producto.codigo.like(search_value),
            Producto.nombre.like(search_value),
            Producto.estado.like(search_value),
            Categoria.nombre.like(search_value),
            Cliente.cedula.like(search_value)
        )
    ).first()
    
    if rresponse is not None:
        return rresponse.serialize
    
    return None

def create_factura(FacturaModel):
    try:
        if Factura.query.filter_by(codigo=FacturaModel['codigo']).first() is not None:
            return 'El codigo {} ya ha sido utilizado para otra factura'.format(FacturaModel['codigo'])
        
        new_bill = Factura(
            codigo=FacturaModel['codigo'],
            metodo_pago=FacturaModel['metodo_pago'],
            fecha_compra=datetime.utcnow(),
            total=0
        )
        
        cliente_item = Cliente.query.filter_by(cedula=FacturaModel['cedula']).first()
        
        if cliente_item is None:
            return "El cliente con identificacion {} no se encontró ".format(FacturaModel['cedula'])
        
        new_bill.cliente = cliente_item
        
        if 'productos' in FacturaModel:
            for producto in FacturaModel['productos']:
                product_item = Producto.query.filter_by(codigo=producto['codigo']).first()
                
                product_item.cantidad -= producto['cantidad']
                sub_total = producto['cantidad'] * product_item.precio
                new_bill.total += sub_total
                factura_productos = Factura_Productos(
                    cantidad=producto['cantidad'],
                    sub_total=sub_total
                )
                factura_productos.producto = product_item
                factura_productos.factura = new_bill
                new_bill.productos.append(factura_productos)
        else:
            return 'debe ingresar al menos un producto'
        
        new_bill.save_to_db()
        return ''
    except Exception as e:
        print(e)
        return str(e)
    
def update_bill(BillModel, codigo):
    try:
        bill = Factura.query.filter_by(codigo=codigo).first()
        Factura_Productos.query.filter_by(factura_id=bill.id).delete()
        if bill is None:
            return "the bill doesn't exist"
        
        
        data = bill.serialize
        for key, value in BillModel.items():
            if key in data and (key != 'productos' or key != 'cedula'):
                data[key] = value
        
        if 'productos' in BillModel:
            bill.total = 0
            for fproducto in bill.productos:
                p = Producto.query.get(fproducto.producto_id)
                p.cantidad += fproducto.cantidad
            
            
            bill.productos = []
            bill.commit()
        
            for producto in BillModel['productos']:
                producto_item = Producto.query.filter_by(codigo=producto['codigo']).first()
                producto_item.cantidad -= producto['cantidad']
                sub_total = producto['cantidad'] * producto_item.precio
                bill.total += sub_total
                factura_productos = Factura_Productos(
                    cantidad=producto['cantidad'],
                    sub_total=sub_total
                )
                factura_productos.producto = producto_item
                factura_productos.factura = bill
                bill.productos.append(factura_productos)
        
        if 'cedula' in BillModel:
            cliente_item = Cliente.query.filter_by(cedula=BillModel['cedula']).first()
            bill.cliente = cliente_item
        
        bill.update_to_db(data)
    
        return ''
    except Exception as e:
        print(e)
        return str(e)
    