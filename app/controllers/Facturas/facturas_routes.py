from . import factura, facturas_service
from flask import make_response, jsonify, request
import json
from helpers import helpers
from flasgger import swag_from


@factura.route('/', methods=['GET'])
def get_all_facturas():
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        bills = facturas_service.get_all_bills()
        response_model['response'] = bills
        if bills is None or len(bills) == 0:
            response_model['message'] = "There aren't register bills yet"
        return jsonify(response_model), 200
    except Exception as e:
        print(e)
        response_model['result'] = False
        response_model['message'] = str(e)
        response_model['response'] = None
        return jsonify(response_model), 500

@factura.route('/<string:search_value>', methods=['GET'])
def get_search_factura(search_value):
    return 

@factura.route('/create', methods=['POST'])
def create_factura():
    """Creacion de facturas

    Returns:
        ResponseModel: 
        
    Ejemplo:
        {
            "codigo": "DDesa",
            "metodo_pago": "Efectivo",
            "productos": [
                {
                    "codigo": "112",
                    "cantidad": 2
                },
                {
                    "codigo": "110",
                    "cantidad": 1
                }
            ],
            "cedula": "1116270722"
        }
    """
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        body = json.loads(request.data)
        re = facturas_service.save_bill(body)

        response_model['message'] = re
            
        return jsonify(response_model), 200
    except Exception as e:
        response_model['message'] = str(e)
        response_model['result'] = False
        response_model['response'] = None
        return jsonify(response_model), 500

@factura.route('/update/<string:codigo>', methods=['PUT'])
def update_factura(codigo):
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        body = json.loads(request.data)
        re = facturas_service.update_bill(body, codigo)
        response_model['message'] = str(re)
        return jsonify(response_model), 200
    except Exception as e:
        response_model['message'] = str(e)
        response_model['result'] = False
        response_model['response'] = None
        return jsonify(response_model), 500
