from . import facturas_repository
from app.controllers.Productos.productos_repository import verify_quanty

def get_all_bills():
    return facturas_repository.get_all()

def get_bill(search):
    if search == '':
        return None
    
    return facturas_repository.get_by_search(search)

def save_bill(BillModel):
    if len(BillModel) != 4:
        return 'Debe ingresar todos los campos'
    
    res = verify_quanty(BillModel['productos'])
    
    if res != '':
        return res
    
    saved = facturas_repository.create_factura(BillModel)
    
    if saved == '':
        return 'The bill has been created successfully'
    
    return str(saved)

def update_bill(BillModel, codigo):
    
    if 'productos' in BillModel:
        res = verify_quanty(BillModel['productos'])
        if res != '':
            return res
    
    updated = facturas_repository.update_bill(BillModel, codigo)
    
    if updated == '':
        return 'The bill has been updated successfully'
    
    return str(updated)
    
    