from flask import Blueprint

from . import clientes_service

cliente = Blueprint(
    'cliente',
    __name__
)

from . import clientes_routes