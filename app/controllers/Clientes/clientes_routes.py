from . import cliente
from . import clientes_service
from flask import make_response, jsonify, request
import json
from helpers import helpers
from flasgger import swag_from
from .specs import create_client, search_client, get_all_clients, delete_client

@cliente.route('/', methods=['GET'])
@swag_from(get_all_clients)
def get_all_clientes():
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        clients = clientes_service.get_all_clients()
        response_model['response'] = clients
        if clients is None or len(clients) == 0:
            response_model['message'] = "There aren't register clients yet"
        return jsonify(response_model), 200
    except Exception as e:
        print(e)
        response_model['result'] = False
        response_model['message'] = str(e)
        response_model['response'] = None
        return jsonify(response_model), 500


@cliente.route('/facturas', methods=['GET'])
def get_all_clientes_by_bills():
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        clients = clientes_service.get_clients_by_number_bill()
        response_model['response'] = clients
        if clients is None or len(clients) == 0:
            response_model['message'] = "There aren't register clients yet"
        return jsonify(response_model), 200
    except Exception as e:
        print(e)
        response_model['result'] = False
        response_model['message'] = str(e)
        response_model['response'] = None
        return jsonify(response_model), 500

@cliente.route('/<string:search_value>', methods=['GET'])
@swag_from(search_client)
def search_cliente(search_value):
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        clients = clientes_service.get_client(search_value)
        response_model['response'] = clients
        if clients is None:
            response_model['message'] = 'the search value does not matches with any result'
        return jsonify(response_model), 200
    except Exception as e:
        print(e)
        response_model['result'] = False
        response_model['message'] = str(e)
        response_model['response'] = None
        return jsonify(response_model), 500

@cliente.route('/create', methods=['POST'])
@swag_from(create_client)
def create_client():
    """Funcion que crea un nuevo cliente

    Returns:
        ResponseModel: objeto que contiene respuesta con result: estado de la transaccion, message: indica el error, response: respuesta en caso de que alla (defecto null)
        
    Ejemplo:
        {
            "cedula": "1116270722",
            "nombre": "Juan Camilo Castaño",
            "direccion": "Calle 38c1 # 18b 116",
            "telefono": "3174006642",
            "foto": ""
        }
    """
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        body = json.loads(request.data)
        re = clientes_service.save_client(body)
        response_model['response'] = str(re)
        if type(re) is str:
            response_model['message'] = re
            response_model['response'] = None
            
        return jsonify(response_model), 201
    except Exception as e:
        response_model['message'] = str(e)
        response_model['result'] = False
        response_model['response'] = None
        return jsonify(response_model), 500
    
@cliente.route('/update/<string:identificacion>', methods=['PUT'])
def update_client(identificacion):
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        body = json.loads(request.data)
        re = clientes_service.update_client(body, identificacion)
        response_model['response'] = re
        return jsonify(response_model), 200
    except Exception as e:
        response_model['message'] = str(e)
        response_model['result'] = False
        response_model['response'] = None
        return jsonify(response_model), 500
    

@cliente.route('/delete/<string:identificacion>', methods=['DELETE'])
@swag_from(delete_client)
def delete_client(identificacion):
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        re = clientes_service.delete_client(identificacion)
        response_model['message'] = re
        return jsonify(response_model), 200
    except Exception as e:
        response_model['message'] = str(e)
        response_model['result'] = False
        response_model['response'] = None
        return jsonify(response_model), 500