from . import clientes_repository

def get_all_clients():
    return clientes_repository.get_all()

def get_clients_by_number_bill():
    return clientes_repository.get_clients_by_number_bills()

def get_client(search):
    if search == '':
        return None
    
    return clientes_repository.get_by_search(search)

def save_client(ClientModel):
    if len(ClientModel) != 5:
        return 'Debe ingresar todos los campos'
    
    saved = clientes_repository.create_cliente(ClientModel)
    
    if saved:
        return 'The client has been created successfully'
    
    return 'An suddenly error has occurrs'

def update_client(ClientModel, identificacion):
    
    updated = clientes_repository.update_cliente(ClientModel, identificacion)

    if updated:
        return 'The client has been updated successfully'
    
    return 'An suddenly error has occurrs'


def delete_client(identificacion):
    
    delete = clientes_repository.delete_cliente(identificacion)
    
    if delete:
        return 'The client has been delete successfully'
    
    return 'An suddenly error has occurrs'
    
    

