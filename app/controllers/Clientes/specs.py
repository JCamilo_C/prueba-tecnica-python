search_client = {
  "tags": ["cliente"],
  "parameters": [
    {
      "name": "search_value",
      "in": "path",
      "type": "string",
      "required": "true",
      "description": "Este valor puede ser identificación, teléfono, dirección, nombre retornara el primero que coincida"
    }
  ],
  "responses": {
    "200": {
      "description": "un response model con un objeto del cliente encontrado",
    }
  }
}

delete_client = {
  "tags": ["cliente"],
  "parameters": [
    {
      "name": "identificacion",
      "in": "path",
      "type": "string",
      "required": "true",
      "description": "identificacion del cliente"
    }
  ],
  "responses": {
    "200": {
      "description": "un response model con un objeto del cliente encontrado",
    }
  }
}

get_all_clients = {
  "tags": ["cliente"],
  "responses": {
    "200": {
      "description": "un response model con una lista de clientes",
    }
  }
}

create_client = {
  "tags": ["cliente"],
  "parameters": [
    {
      "name": "cedula",
      "in": "body",
      "type": "string",
      "required": "true"
    },
    {
      "name": "nombre",
      "in": "body",
      "type": "string",
      "required": "true"
    },
    {
      "name": "direccion",
      "in": "body",
      "type": "string",
      "required": "true"
    },
    {
      "name": "telefono",
      "in": "body",
      "type": "string",
      "required": "true"
    },
    {
      "name": "foto",
      "in": "body",
      "type": "string",
      "required": "true",
      "description": "la foto debe de ser enviada en base64 o url"
    }
  ],
  "responses": {
    "201": {
      "description": "un response model con una respuesta de creacion de cliente",
      
    },
    "examples": {
        "cedula": "1116270722",
        "nombre": "Juan Camilo Castaño",
        "direccion": "Calle 38c1 # 18b 116",
        "telefono": "3174006642",
        "foto": ""
      }
  }
}