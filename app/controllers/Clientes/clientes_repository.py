from app.models.models import Cliente, Factura
from sqlalchemy import or_, update, func, desc
from app import db


def get_all():
    rresponse = [r.serialize for r in db.session.query(Cliente).all()]
    return rresponse

def get_clients_by_number_bills():
    rresponse = Cliente.query.outerjoin(Factura).group_by(Cliente.id).order_by(db.func.count(Factura.id).desc())
    clients = [c.bill_serialize for c in rresponse]
    
    return clients

def get_by_search(search_value):
    rresponse = Cliente.query.filter(
        or_(
            Cliente.cedula.like(search_value),
            Cliente.nombre.like(search_value),
            Cliente.direccion.like(search_value),
            Cliente.telefono.like(search_value)
        )
    ).first()
    if rresponse is not None:
        return rresponse.serialize
    
    return None

def create_cliente(ClientModel):
    try:
        new_client = Cliente(**ClientModel)
        new_client.save_to_db()
        
        return True
    except Exception as e:
        print(e)
        return False
    
def update_cliente(ClientModel, identificacion):
    try:
        client = Cliente.query.filter_by(cedula=identificacion).first()
        data = client.serialize
        for key, value in ClientModel.items():
            if key in data:
                data[key] = value
        
        client.update_to_db(data)
        return True
    except Exception as e:
        print(e)
        return False

def delete_cliente(identificacion):
    try:
        client = Cliente.query.filter_by(cedula=identificacion).first()
        client.delete_to_db()
        return True
    except Exception as e:
        print(e)
        return False