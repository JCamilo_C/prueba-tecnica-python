from app.models.models import Producto, Categoria
from sqlalchemy import or_, update
from app import db


def get_all():
    rresponse = [r.serialize for r in Producto.query.filter_by(estado=True).all()]
    return rresponse

def get_by_search(search_value):
    rresponse = Producto.query.join(Producto.categorias).filter(
        or_(
            Producto.codigo.like(search_value),
            Producto.nombre.like(search_value),
            Producto.estado.like(search_value),
            Categoria.nombre.like(search_value)
        )
    ).first()
    
    if rresponse is not None:
        return rresponse.serialize
    
    return None

def create_producto(ProductoModel):
    try:
        if Producto.query.filter_by(codigo=ProductoModel['codigo']).first() is not None:
            return 'El codigo {} ya ha sido utilizado para otro producto'.format(ProductoModel['codigo'])
        
        new_product = Producto(
            codigo=ProductoModel['codigo'],
            nombre=ProductoModel['nombre'],
            precio=ProductoModel['precio'],
            cantidad=ProductoModel['cantidad'],
            estado=ProductoModel['estado']
        )
        
        for categoria in ProductoModel['categorias']:
            categoria_item = Categoria.query.filter_by(nombre=categoria).first()
            if categoria_item is None:
                categoria_item = Categoria(nombre=categoria)
            new_product.categorias.append(categoria_item)
          
        new_product.save_to_db()
        return ''
    except Exception as e:
        print(e)
        return str(e)
    
def update_producto(ProductModel, codigo):
    try:
        product = Producto.query.filter_by(codigo=codigo).first()
        
        if product is None:
            return "the product doesn't exist"
        
        
        data = product.serialize
        for key, value in ProductModel.items():
            if key in data and key != 'categorias':
                data[key] = value
        
        if 'categorias' in ProductModel:
            product.categorias = []
            product.commit()
        
            for categoria in ProductModel['categorias']:
                categoria_item = Categoria.query.filter_by(nombre=categoria).first()
                if categoria_item is None:
                    categoria_item = Categoria(nombre=categoria)
                product.categorias.append(categoria_item)
        
        product.update_to_db(data)
    
        return ''
    except Exception as e:
        print(e)
        return str(e)
    
def deactivate_product(codigo):
    try:
        product = Producto.query.filter_by(codigo=codigo).first()
        if product is None:
            return "the product doesn't exist"
        
        product.change_status()
        return ''
    except Exception as e:
        print(e)
        return str(e)
    
def verify_quanty(productos):
    try:
        for producto in productos:
            product = Producto.query.filter_by(codigo=producto['codigo']).first()
            
            if product is None:
                return 'El producto {} no existe'.format(producto['codigo'])

            if product.cantidad < producto['cantidad']:
                 return 'El producto {} tiene menor cantidad a la ingresada'.format(producto['codigo'])
        return ''
    except Exception as e:
        print(e)
        return str(e)