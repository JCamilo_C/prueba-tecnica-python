from . import productos_repository

def get_all_products():
    return productos_repository.get_all()

def get_product(search):
    if search == '':
        return None
    
    return productos_repository.get_by_search(search)

def save_product(ProductModel):
    if len(ProductModel) != 6:
        return 'Debe ingresar todos los campos'
    
    saved = productos_repository.create_producto(ProductModel)
    
    if saved == '':
        return 'The product has been created successfully'
    
    return str(saved)

def update_product(ProductModel, codigo):
    
    updated = productos_repository.update_producto(ProductModel, codigo)
    
    if updated == '':
        return 'The product has been updated successfully'
    
    return str(updated)
    
def deactivate_product(codigo):
    delete = productos_repository.deactivate_product(codigo)
    
    if delete == '':
        return 'The product has been created successfully'
    
    return str(delete)