from . import producto
from . import productos_service
from flask import make_response, jsonify, request
import json
from helpers import helpers

@producto.route('/', methods=['GET'])
def get_all_productos():
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        products = productos_service.get_all_products()
        response_model['response'] = products
        if products is None or len(products) == 0:
            response_model['message'] = "There aren't register products yet"
        return jsonify(response_model), 200
    except Exception as e:
        print(e)
        response_model['result'] = False
        response_model['message'] = str(e)
        response_model['response'] = None
        return jsonify(response_model), 500


@producto.route('/<string:search_value>', methods=['GET'])
def search_producto(search_value):
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        products = productos_service.get_product(search_value)
        response_model['response'] = products
        if products is None:
            response_model['message'] = 'the search value does not matches with any result'
        return jsonify(response_model), 200
    except Exception as e:
        print(e)
        response_model['result'] = False
        response_model['message'] = str(e)
        response_model['response'] = None
        return jsonify(response_model), 500

@producto.route('/create', methods=['POST'])
def create_producto():
    """Funcion que crear un nuevo producto

    Args:
        ProductoModel (Producto): Nuevo producto

    Returns:
        ResponseModel: objeto que contiene respuesta con result: estado de la transaccion, message: indica el error, response: respuesta en caso de que alla (defecto null)
        
    Ejemplo:
        {
            "codigo": "112",
            "nombre": "Camiseta",
            "precio": 12500,
            "cantidad": 5,
            "estado": true,
            "categorias": ["Telas", "Sport"]
        }
    """
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        body = json.loads(request.data)
        re = productos_service.save_product(body)

        response_model['message'] = re
            
        return jsonify(response_model), 200
    except Exception as e:
        response_model['message'] = str(e)
        response_model['result'] = False
        response_model['response'] = None
        return jsonify(response_model), 500
    
@producto.route('/update/<string:codigo>', methods=['PUT'])
def update_product(codigo):
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        body = json.loads(request.data)
        re = productos_service.update_product(body, codigo)
        response_model['message'] = str(re)
        return jsonify(response_model), 200
    except Exception as e:
        response_model['message'] = str(e)
        response_model['result'] = False
        response_model['response'] = None
        return jsonify(response_model), 500
    
@producto.route('/delete/<string:codigo>', methods=['DELETE'])
def deactivate_producto(codigo):
    response_model = {
        'response': None,
        'result': True,
        'message': ''
    }
    try:
        re = productos_service.deactivate_product(codigo)
        response_model['message'] = str(re)
        return jsonify(response_model), 200
    except Exception as e:
        response_model['message'] = str(e)
        response_model['result'] = False
        response_model['response'] = None
        return jsonify(response_model), 500