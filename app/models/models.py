from app import db
from sqlalchemy import DateTime
from sqlalchemy.orm import relationship


class Factura_Productos(db.Model):
    __tablename__ = 'factura_producto'
    
    factura_id = db.Column(db.Integer, db.ForeignKey('factura.id'), primary_key=True)
    producto_id = db.Column(db.Integer, db.ForeignKey('producto.id'), primary_key=True)
    factura = db.relationship("Factura", back_populates="productos")
    producto = db.relationship("Producto", back_populates="facturas")
    cantidad = db.Column('cantidad', db.Integer, nullable=False)
    sub_total = db.Column('sub_total', db.Float(2), nullable=False)
    
    @property
    def serialize(self):
        return {
            'factura_id': self.factura.codigo,
            'producto_id': self.producto.codigo,
            'cantidad': self.cantidad,
            'sub_total': self.sub_total
        }


class Factura(db.Model):
    __tablename__ = 'factura'
    
    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.String(80), unique=True, nullable=False)
    metodo_pago = db.Column(db.String(80), nullable=False)
    
    cliente_id = db.Column(db.Integer, db.ForeignKey('cliente.id'), nullable=False)
    cliente = db.relationship('Cliente',  back_populates="factura")
    
    productos = db.relationship("Factura_Productos")
    
    total = db.Column(db.Float(2), nullable = False)
    fecha_compra = db.Column(DateTime, nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        
    def update_to_db(self, data):
        self.codigo = data['codigo']
        self.metodo_pago = data['metodo_pago']
        self.total = data['total']
        
        db.session.merge(self)
        db.session.commit()

    
    def delete_to_db(self):
        db.session.delete(self)
        db.session.commit()
        
    def commit(self):
        db.session.commit()
        
    @property
    def serialize(self):
        return {
            'id': self.id,
            'codigo': self.codigo,
            'metodo_pago': self.metodo_pago,
            'cliente': self.cliente.serialize,
            'productos': [s.serialize for s in self.productos],
            'total': self.total
        }


    def __repr__(self):
        return '<Factura %r>' % self.codigo
    
class Cliente(db.Model):
    __tablename__ = 'cliente'
    
    id = db.Column(db.Integer, primary_key=True)
    cedula = db.Column(db.String(80), unique=True, nullable=False)
    nombre = db.Column(db.String(80), nullable=False)
    direccion = db.Column(db.String(80), nullable=False)
    telefono = db.Column(db.String(80),  nullable=False)
    foto = db.Column(db.String(120), nullable=False)
    
    factura = relationship("Factura", back_populates="cliente", uselist=True)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_to_db(self):
        db.session.delete(self)
        db.session.commit()

    def update_to_db(self, data):
        self.cedula = data['cedula']
        self.direccion = data['direccion']
        self.foto = data['foto']
        self.nombre = data['nombre']
        self.telefono = data['telefono']
        db.session.merge(self)
        db.session.commit()

    def __repr__(self):
        return '<Cliente %r>' % self.nombre

    @property
    def serialize(self):
        return {
            'id': self.id,
            'cedula': self.cedula,
            'nombre': self.nombre,
            'direccion': self.direccion,
            'telefono': self.telefono,
            'foto': self.foto
        }
        
    @property
    def bill_serialize(self):
        return {
            'id': self.id,
            'cedula': self.cedula,
            'nombre': self.nombre,
            'direccion': self.direccion,
            'telefono': self.telefono,
            'foto': self.foto,
            'facturas': [b.serialize for b in self.factura]
        }


categorias = db.Table('productos_categorias',
    db.Column('categoria_id', db.Integer, db.ForeignKey('categoria.id'), primary_key=True),
    db.Column('producto_id', db.Integer, db.ForeignKey('producto.id'), primary_key=True)
)

    
class Producto(db.Model):
    __tablename__ = 'producto'
    
    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.String(80), unique=True, nullable=False)
    nombre = db.Column(db.String(80), nullable=False)
    precio = db.Column(db.Float(2), nullable=False)
    cantidad = db.Column(db.Integer,  nullable=False)
    estado = db.Column(db.Boolean(True), nullable=False)
    
    facturas = relationship("Factura_Productos", back_populates="producto")
    categorias = db.relationship('Categoria', secondary=categorias, lazy='subquery',
                            backref=db.backref('productos', lazy=True))

    def update_to_db(self, data):
        self.codigo = data['codigo']
        self.precio = data['precio']
        self.cantidad = data['cantidad']
        self.nombre = data['nombre']
        self.estado = data['estado']
        
        db.session.merge(self)
        db.session.commit()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def change_status(self):
        self.estado = False
        
        db.session.merge(self)
        db.session.commit()

    def commit(self):
        db.session.commit()
    def __repr__(self):
        return '<Producto %r>' % self.nombre
    
    @property
    def serialize(self):
        return {
            'id': self.id,
            'codigo': self.codigo,
            'nombre': self.nombre,
            'estado': self.estado,
            'precio': self.precio,
            'cantidad': self.cantidad,
            'categorias': [c.serialize for c in self.categorias]
        }
    
    
class Categoria(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), nullable=False)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @property
    def serialize(self):
        return {
            'id': self.id,
            'nombre': self.nombre,
        }
    

    def __repr__(self):
        return '<Category %r>' % self.name