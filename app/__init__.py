from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import config
from flasgger import Swagger


def get_app():
    app = Flask(__name__)
    app.config.from_pyfile('../config/config.py')
    return app

app = get_app()
db = SQLAlchemy(app)


def create_app():
    app = get_app()

    from .models import models
    db.init_app(app)
    db.create_all()
    db.session.commit()

    # start swagger
    swagger = Swagger(app)
    
    from flask_cors import CORS
    CORS(app)
    
    
    # Register routes
    from .controllers.Clientes import cliente
    app.register_blueprint(cliente, url_prefix='/clientes')
    
    from .controllers.Productos import producto
    app.register_blueprint(producto, url_prefix='/productos')
    
    from .controllers.Facturas import factura
    app.register_blueprint(factura, url_prefix='/facturas')
    
    return app