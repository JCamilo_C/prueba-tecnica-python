import os
from pathlib import Path

DEBUG = True
TESTING = True
API_URL = 'http://localhost:5200'

SQLALCHEMY_DATABASE_URI = 'sqlite:///tsaka.sqlite3'
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_COMMIT_ON_TEARDOWN = True